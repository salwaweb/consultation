var localVideo;
var localStream;
var remoteVideo;
var peerConnection;
var uuid;
var serverConnection;
var targetUsername = "kkkk";
var userToCall;
var MyStream;
var RDVID;
var msgAnswer;


var hasAddTrack = false;
var targetUsername = null; 
var mediaConstraints = {
  audio: true, // We want an audio track
  video: true // ...and we want a video track
};

var peerConnectionConfig = {
  'iceServers': [
    {'urls': 'stun:stun.stunprotocol.org:3478'},
    {'urls': 'stun:stun.l.google.com:19302'},
  ]
};

function pageReady(username) {


serverConnection = new WebSocket("ws://localhost:8080");


    uuid = username;
  serverConnection.onmessage = gotMessageFromServer;
  
 //serverConnection.onopen = sendToServer({type: 'login',from: "ffff",id:  username });

  console.log("login send");
	
  
  	var constraints = {
    video: true,
    audio: true
  };
  
}

function setuser(type){
	document.cookie ='userID='+type+'; expires=Fri, 3 Aug 2020 20:47:11 UTC; path=/'
	uuid = type;
}

function getUserMediaSuccess(stream) {
	console.log("my sttream");
  localStream = stream;
  try{
	localVideo.src = stream;
  }catch{
	localVideo.srcObject = stream;
  }
  
}

function gotMessageFromServer(message) {

  var signal = JSON.parse(message.data);
 // var signal = message.type;
	console.log("****************************")
    console.log(signal.type)
	var msg = signal;
	
	switch(msg.type) {
		case 'login':
			console.log("user successfully connected");
			console.log(signal);
            sendToServer({type: 'login',from: "ffff",id:  uuid });
			break;

		case "user_horsligne":
			alert("user hors ligne");
			closeVideoCall();
			break;
	
		case "user_busy":
			alert("user busy with other call");
			closeVideoCall();
			break;

      case "rejectusername":
        break;

      case "userlist":      // Received an updated user list
        handleUserlistMsg(msg);
        break;
		
      case "user-connecter":      // Received an updated user list
		console.log("user-connecter");
        handleUserlistMsg(msg);
        break;

      // Signaling messages: these messages are used to trade WebRTC
      // signaling information during negotiations leading up to a video
      // call.

      case "video-offer":  // Invitation and offer to chat
		console.log("video-offer");
          $("#myModal").modal('show');
          msgAnswer = msg;

        break;

      case "video-answer":  // Callee has answered our offer
        handleVideoAnswerMsg(msg);
        break;

      case "new-ice-candidate": // A new ICE candidate has been received
          console.log("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&")
        handleNewICECandidateMsg(msg);
        break;

      case "hang-up": // The other peer has hung up the call
        handleHangUpMsg(msg);
        break;

        case "check_for_notifcation":
            //$("#10").css("color", "red");
            console.log("check_for_notifcation")
            $("#user_profile_notif").css("background-color", "aqua")
            $("#notif_rdv").css("background-color", "aqua")
            $("#alert_txt").text("you have a seance who will start soon. Go check your profile !!!!")
            $(".myAlert-top").show();
            setTimeout(function(){
                $(".myAlert-top").hide();
            }, 20000);
        break;

      // Unknown message; output to console for debugging.

      default:
        //log_error("Unknown message received:");
        //log_error(msg);
    }

}

function muteCall(type){
	if(type == 1){
		MyStream.getAudioTracks()[0].enabled = !(MyStream.getAudioTracks()[0].enabled);
	}else{
		MyStream.getVideoTracks()[0].enabled = !(MyStream.getVideoTracks()[0].enabled);
	}
	
}

function handleUserlistMsg(msg) {
  var i;

  var listElem = document.getElementById("users");

  // Remove all current list members. We could do this smarter,
  // by adding and updating users instead of rebuilding from
  // scratch but this will do for this sample.

  // while (listElem.firstChild) {
    // listElem.removeChild(listElem.firstChild);
  // }
  
  $("#users option").remove();

  // Add member names from the received list
  
 $('#users')
         .append($("<option></option>")
         .attr("value",0)
         .text(""));
  
 
  for (i=0; i < msg.users.length; i++) {
	  if(msg.users[i] != uuid){
		  $('#users')
         .append($("<option></option>")
         .attr("value",i+1)
         .text(msg.users[i]));
	  }
     
  }
}

function userConnected(){
	
	$("#content").show();
	$("#login").hide();
	
	localVideo = document.getElementById('localVideo');
	remoteVideo = document.getElementById('remoteVideo');
  
}

function handleNewICECandidateMsg(msg) {
	console.log("handleNewICECandidateMsg")
  var candidate = new RTCIceCandidate(msg.candidate);

  peerConnection.addIceCandidate(candidate)
    .catch(reportError);
}

function handleVideoOfferMsg() {
    var msg = msgAnswer;
    console.log("-/-/-/-/-/-/-/-/-/-")

	console.log("/*/*/*/*/*/*/*/*/*/*/*/*/");
	console.log(msg)
	console.log(msg.to)
	console.log(msg.from)
	userToCall = msg.from;
	var to = msg.to;
	var from = msg.from;

    RDVID = msg.rdv;

  var localStream = null;
  peerConnection = new RTCPeerConnection(peerConnectionConfig);
  hasAddTrack = (peerConnection.addTrack !== undefined);
  peerConnection.onicecandidate = handleICECandidateEvent;
  peerConnection.ontrack = handleTrackEvent;
  //peerConnection.onnegotiationneeded = handleNegotiationNeededEvent;
  peerConnection.onremovetrack = handleRemoveTrackEvent;
  peerConnection.oniceconnectionstatechange = handleICEConnectionStateChangeEvent;
  peerConnection.onicegatheringstatechange = handleICEGatheringStateChangeEvent;
  peerConnection.onsignalingstatechange = handleSignalingStateChangeEvent;
  
  // We need to set the remote description to the received SDP offer
  // so that our local WebRTC layer knows how to talk to the caller.

  var desc = new RTCSessionDescription(msg.sdp);

  peerConnection.setRemoteDescription(desc).then(function () {
    console.log("Setting up the local media stream...");
    return navigator.mediaDevices.getUserMedia(mediaConstraints);
	
  })
  .then(function(stream) {
    console.log("-- Local video stream obtained");
    localStream = stream;
	MyStream = stream;
    document.getElementById("localVideo").src = localStream;
    document.getElementById("localVideo").srcObject = localStream;

    if (hasAddTrack) {
      console.log("-- Adding tracks to the RTCPeerConnection");
      localStream.getTracks().forEach(track =>
            peerConnection.addTrack(track, localStream)
      );
    } else {
      console.log("-- Adding stream to the RTCPeerConnection");
      peerConnection.addStream(localStream);
    }
  })
  .then(function() {
    console.log("------> Creating answer");
    // Now that we've successfully set the remote description, we need to
    // start our stream up locally then create an SDP answer. This SDP
    // data describes the local end of our call, including the codec
    // information, options agreed upon, and so forth.
    return peerConnection.createAnswer();
  })
  .then(function(answer) {
    console.log("------> Setting local description after creating answer");
    // We now have our answer, so establish that as the local description.
    // This actually configures our end of the call to match the settings
    // specified in the SDP.
    return peerConnection.setLocalDescription(answer);
  })
  .then(function() {
    var msg = {
      name: "hhhhh",
      target: targetUsername,
      type: "video-answer",
      rdv: RDVID,
	  from: to,
	  to : from,
      sdp: peerConnection.localDescription
    };

    // We've configured our end of the call now. Time to send our
    // answer back to the caller so they know that we want to talk
    // and how to talk to us.

    console.log("Sending answer packet back to other peer");
    sendToServer(msg);
  })
  .catch(handleGetUserMediaError);

		
}

function handleVideoAnswerMsg(msg) {


	console.log("handleVideoAnswerMsg")
	//confirm("user racrocher");
  console.log("Call recipient has accepted our call");

  // Configure the remote description, which is the SDP payload
  // in our "video-answer" message.
  
  peerConnection.onicecandidate = handleICECandidateEvent;

  var desc = new RTCSessionDescription(msg.sdp);
  peerConnection.setRemoteDescription(desc).catch(reportError);

}


function gotIceCandidate(event) {
	console.log("gotIceCandidate");
	console.log(event);
  if(event.candidate != null) {
	  console.log("candidate not null ");
    serverConnection.send(JSON.stringify({'ice': event.candidate, 'uuid': uuid, 'from' : uuid}));
  }
}

function handleICECandidateEvent(event) {
    console.log("handleICECandidateEvent")


  if (event.candidate) {

    sendToServer({
      type: "new-ice-candidate",
	  from: uuid,
	  to: userToCall,
      candidate: event.candidate
    });
  }
}

function handleTrackEvent(event) {
	console.log("handleTrackEvent");
  document.getElementById("remoteVideo").srcObject = event.streams[0];
  document.getElementById("hangup-button").disabled = false;
}

function createdDescription(description) {
  console.log('got description');
  console.log(description);

  peerConnection.setLocalDescription(description).then(function() {
    serverConnection.send(JSON.stringify({'sdp': peerConnection.localDescription, 'uuid': uuid, 'from': uuid}));
  }).catch(errorHandler);
}

function gotRemoteStream(event) {
  console.log('got remote stream');
  console.log(event)
  remoteVideo.srcObject = event.streams[0];
}

function errorHandler(error) {
  console.log(error);
}

function createUUID() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000).toString(16).substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

function createPeerConnection() {

  peerConnection = new RTCPeerConnection(peerConnectionConfig);
    hasAddTrack = (peerConnection.addTrack !== undefined);
  peerConnection.ontrack = handleTrackEvent;
  peerConnection.onnegotiationneeded = handleNegotiationNeededEvent;
  peerConnection.onremovetrack = handleRemoveTrackEvent;
  peerConnection.oniceconnectionstatechange = handleICEConnectionStateChangeEvent;
  peerConnection.onicegatheringstatechange = handleICEGatheringStateChangeEvent;
  peerConnection.onsignalingstatechange = handleSignalingStateChangeEvent;
  
  // Because the deprecation of addStream() and the addstream event is recent,
  // we need to use those if addTrack() and track aren't available.

  if (hasAddTrack) {
    peerConnection.ontrack = handleTrackEvent;
  } else {
    peerConnection.onaddstream = handleAddStreamEvent;
  }
}

function invite(usertoCall, rdv_id) {

  if (peerConnection) {
    alert("You can't start a call because you already have one open!");
  } else {
    
    //userToCall = $("#user_id_tocall").val();
	userToCall = usertoCall;
      RDVID = rdv_id;
	console.log(userToCall);

	createPeerConnection();

    navigator.mediaDevices.getUserMedia(mediaConstraints)
    .then(function(localStream) {
		
		MyStream = localStream;
		document.getElementById("localVideo").srcObject = localStream;
		localStream.getTracks().forEach(track => peerConnection.addTrack(track, localStream));
    })
    .catch(handleGetUserMediaError);
	
  }
}
var call = 0;
function handleNegotiationNeededEvent() {
	console.log("handleNegotiationNeededEvent")
	 if(call === 0 )
    {
        call++;
    }
    else
    {
        return;
    }
	
  console.log("*** Negotiation needed");

  console.log("---> Creating offer");
  peerConnection.createOffer().then(function(offer) {
    console.log("---> Creating new description object to send to remote peer");
    return peerConnection.setLocalDescription(offer);
  })
  .then(function() {
    console.log("---> Sending offer to remote peer");
    sendToServer({
		from : uuid,
		to : userToCall,
        rdv: RDVID,
		type: "video-offer",
		sdp: peerConnection.localDescription
    });
  })
  .catch(reportError);
}

function handleRemoveTrackEvent(event) {
  var stream = document.getElementById("remoteVideo").srcObject;
  var trackList = stream.getTracks();
 
  if (trackList.length == 0) {
    closeVideoCall();
  }
}

function closeVideoCall() {
  var remoteVideo = document.getElementById("remoteVideo");
  var localVideo = document.getElementById("localVideo");
	call = 0;
  if (peerConnection) {
    peerConnection.ontrack = null;
    peerConnection.onremovetrack = null;
    peerConnection.onremovestream = null;
    peerConnection.onnicecandidate = null;
    peerConnection.oniceconnectionstatechange = null;
    peerConnection.onsignalingstatechange = null;
    peerConnection.onicegatheringstatechange = null;
    peerConnection.onnegotiationneeded = null;

    if (remoteVideo.srcObject) {
      remoteVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    if (localVideo.srcObject) {
      localVideo.srcObject.getTracks().forEach(track => track.stop());
    }

    peerConnection.close();
    peerConnection = null;
  }

  userToCall = null;
  MyStream = null;
  remoteVideo.removeAttribute("src");
  remoteVideo.removeAttribute("srcObject");
  localVideo.removeAttribute("src");
  remoteVideo.removeAttribute("srcObject");

  //document.getElementById("hangup-button").disabled = true;
  targetUsername = null;
}

function handleICEConnectionStateChangeEvent(event) {
  switch(peerConnection.iceConnectionState) {
    case "closed":
    case "failed":
    case "disconnected":
      closeVideoCall();
      break;
  }
}

function handleICEGatheringStateChangeEvent(event) {
  // Our sample just logs information to console here,
  // but you can do whatever you need.
}

function handleSignalingStateChangeEvent(event) {
  switch(peerConnection.signalingState) {
    case "closed":
      closeVideoCall();
      break;
  }
};

function handleGetUserMediaError(e) {
  switch(e.name) {
    case "NotFoundError":
      alert("Unable to open your call because no camera and/or microphone" +
            "were found.");
      break;
    case "SecurityError":
    case "PermissionDeniedError":
      // Do nothing; this is the same as the user canceling the call.
      break;
    default:
      alert("Error opening your camera and/or microphone: " + e.message);
      break;
  }

  closeVideoCall();
}

function hangUpCall() {
  closeVideoCall();
  sendToServer({
    name: "hhh",
	from: uuid,
    to: userToCall,
      rdv: RDVID,
    target: targetUsername,
    type: "hang-up"
  });
}

function handleHangUpMsg() {
  closeVideoCall();
 
}

function sendToServer(msg) {
    console.log("send to server");
	console.log(msg)
  var msgJSON = JSON.stringify(msg);
	//msgJSON.from = uuid;
  serverConnection.send(msgJSON);
}

function reportError(errMessage) {
  console.log("Error " + errMessage.name + ": " + errMessage.message);
}



