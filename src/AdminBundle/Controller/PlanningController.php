<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Specialite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;

use AdminBundle\Entity\Seance;


/**
 * Planning controller.
 *
 */
class PlanningController extends Controller
{

    public function indexAction(Request $request)
    {
        //$userid = $request->get("userid");

        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $duree=$user->getDuree();
        $em = $this->getDoctrine()->getManager();
       // $user = $em->getRepository("AdminBundle:User")->find($userid);
        $seance = $em->getRepository("AdminBundle:Seance")->findBy(array("user" => $user));

        $now = new \DateTime();
        $day_of_week = $now->format('N');

        $arr = array();
        foreach($seance as $key=>$value){
            $schedule = array();
            $schedule["id"] = $value->getId();
            $schedule["title"] = "seance";
            $schedule["weekday"] = $value->getDay();
            $schedule["color"] = '#ffffff';
            $schedule["isVisible"] = true;
            $schedule["bgColor"] = '#69BB2D';
            $schedule["dragBgColor"] = '#69BB2D';
            $schedule["borderColor"] = '#69BB2D';
            $schedule["calendarId"] = 'logged-workout';
            $schedule["category"] = 'time';
            $schedule["type"] = $value->getTimeType();

            if($value->getDay() > $day_of_week){
                $dayToAdd = ABS($value->getDay() - $day_of_week);
                $date = strtotime("+".$dayToAdd." day");
                $xx = date('Y-m-d', $date);
                $schedule["start"] = date('Y-m-d', $date)." ". $value->getTime()->format('H:i:s');
                $schedule["end"] = date('Y-m-d', $date)." ". $value->getEnd()->format('H:i:s');
            }else if($value->getDay() == $day_of_week){

                $now = date("Y-m-d");
                $schedule["start"] = $now." ". $value->getTime()->format('H:i:s');
                $schedule["end"] = $now." ". $value->getEnd()->format('H:i:s');
            }else{

                $dayToAdd = (7 - $day_of_week ) + $value->getDay();

                $date = strtotime("+".$dayToAdd." day");
                $xx = date('Y-m-d', $date);
                $schedule["start"] = date('Y-m-d', $date)." ". $value->getTime()->format('H:i:s');
                $schedule["end"] = date('Y-m-d', $date)." ". $value->getEnd()->format('H:i:s');
            }

            $arr[] = $schedule;
        }


        $response = json_encode(array('result' => $arr, "result2" => $duree));
        return new Response($response,200, array(
            'Content-Type' => 'application/json'
        ));


    }

    public function addSeanceAction(Request $request)
    {
        $data = json_decode($request->get("data"));

        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
       // $user = $em->getRepository("AdminBundle:User")->find($userid);
        $start = new \DateTime($data->start->_date);
        $endDate = new \DateTime($data->end->_date);
        $type=$request->get($data)->getType();

        $seance = new Seance();

        $seance->setTime($start);
        $seance->setEnd($endDate);
        $seance->setTimeType($type);
        $seance->setDay($start->format("N"));
        $seance->setStatus(1);
        $seance->setUser($user);
        $seance->setTimeType($data->type);

        $em->persist($seance);
        $em->flush();

        $response = json_encode(array('result' => $user, "result2" => $start, "res" => $data->start->_date));
        return new Response($response,200, array(
            'Content-Type' => 'application/json'
        ));


    }

    public function editSeanceAction(Request $request, Seance $id)
    {
        $data = json_decode($request->get("data"));

        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
       // $user = $em->getRepository("AdminBundle:User")->find($userid);
        $start = new \DateTime($data->start->_date);
        $endDate = new \DateTime($data->end->_date);
        $seance = $em->getRepository("AdminBundle:Seance")->findBy(array( "id" => $id ,"user" => $user));
$type=$request->get($data)->getType();
        //dd($seance);
        $seance->setTime($start);
        $seance->setEnd($endDate);
        $seance->setTimeType($type);
        $seance->setDay($start->format("N"));
        $seance->setTimeType($data->type);

        $em->persist($seance);
        $em->flush();

        $response = json_encode(array('result' => $user, "result2" => $start, "res" => $data->start->_date));
        return new Response($response,200, array(
            'Content-Type' => 'application/json'
        ));


    }


}
