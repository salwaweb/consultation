<?php

namespace AdminBundle\Controller;

use AdminBundle\Entity\Rdv;
use AdminBundle\Entity\User;
use AdminBundle\Entity\Seance;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\Routing\Annotation\Route;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        return $this->render('AdminBundle:Default:index.html.twig',array(
            'user'=>$user->getId()
        ));
    }

    /**
     * @Route("/planning", name="planning")
     */
    public function planningAction()
    {

        // replace this example code with whatever you need
        return $this->render('AdminBundle:Professionnel:planning.html.twig');
    }


    public function mesrdvAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $rdvs = $em->getRepository('AdminBundle:Rdv')->findBy(array('prof'=>$user));
        return $this->render('AdminBundle:Professionnel:mes-rdv.html.twig',array(
            'rdvs'=>$rdvs,
            'user'=>$user->getId()
        ));

    }
    /**
     * @Route("/rendez-vous", name="pro-rdv")
     */
    public function rendezVousAction()
    {
        // replace this example code with whatever you need
        return $this->render('AdminBundle:Professionnel:planning.html.twig');
    }
    public function profilAction()
    {
        // replace this example code with whatever you need
        return $this->render('AdminBundle:Professionnel:profile.html.twig');
    }

    public function modifAction(Request $request, User $user)
    {

        $editForm = $this->createForm('AdminBundle\Form\UserEditType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('user_edit', array('id' => $user->getId()));
        }

        return $this->render('AdminBundle:Professionnel:modif.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),

        ));
    }
    public function homeAction()
    {
        return $this->render('AdminBundle:Default:index.html.twig');
    }
    public function addseanceAction()
    {


        $utilisateur = $this->container->get('security.token_storage')->getToken()->getUser();
        $seance = new Seance();
        $seance->setUser($utilisateur);
        $seance->setTime(new \DateTime());
        $seance->setStatus(true);
        $seance->setDay(new \DateTime());
        $em->persist($seance);
        $em->flush();

    }

    public function addrendevAction(Seance $seance, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $part = $this->container->get('security.token_storage')->getToken()->getUser();
        $seance = $em->getRepository('AdminBundle:Seance')->find($request->get($seance));

        $prof= $seance->getUser();

        $rdv = new Rdv();
        $rdv->setParticulier($part);
        $rdv->setProf($prof);
        $rdv->setSeance($seance);

        $rdv->setPayee(true);
        $rdv->setOrd($request->get('ordonnance'));
        $rdv->setPartPresent($request->get('part'));
        $rdv->setProfPresent($request->get('prof'));
        $rdv->setRemain(new \DateTime());
        $em->persist($rdv);
        $em->flush();

    }

    public function addfactureAction(Rdv $rdv, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $part = $this->container->get('security.token_storage')->getToken()->getUser();
        $rdv =  $em->getRepository('AdminBundle:Rdv')->find($request->get($rdv));

        $facture = new Facture();
        $facture->setUser($part);
        $facture->setRdv($rdv);
        $facture->setPrix($request->get('prix'));
        $facture->setDuree(new \DateTime());
        $facture->setNumfact($request->get('numero'));
        $em->persist($facture);
        $em->flush();

    }
    public function addhistoriqueAction(Rdv $rdv, Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $rdv =  $em->getRepository('AdminBundle:Rdv')->find($request->get($rdv));
        $userdec= $rdv->getProf();
        $historique= new RdvHist();
        $historique->setUserDecroche($userdec);
        $historique->setIdRdv($rdv);
        $historique->setDateStart(new \DateTime());
        $historique->setDateEnd(new \DateTime());
        $em->persist($historique);
        $em->flush();


    }


}
