<?php
// src/AdminBundle/Entity/User.php

namespace AdminBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;



    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;

    /**
     * @var string
     *
     * @ORM\Column(name="sex", type="string", length=255, nullable=true)
     */
    private $sex;

    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    private $adresse;

    /**
     * @var string
     *
     * @ORM\Column(name="tel", type="string", length=255, nullable=true)
     */
    private $tel;

    /**
     * @var int
     *
     * @ORM\Column(name="duree", type="integer", nullable=true)
     */
    private $duree;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", nullable=true)
     */
    private $prix;
    /**
     * @var \Specialite
     *
     * @ORM\ManyToOne(targetEntity="Specialite")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="specialite", referencedColumnName="id")
     * })
     */
    private $specialite;
    /**
     * @var text
     *
     * @ORM\Column(name="spec", type="text", nullable=true)
     */
    private $spec;
    /**
     * @var int
     *
     * @ORM\Column(name="numrib", type="integer", nullable=true)
     */
    private $numrib;
    /**
     * @var int
     *
     * @ORM\Column(name="edinar", type="integer", nullable=true)
     */
    private $edinar;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $cv;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $diplome;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $cin;
    /**
     * @ORM\OneToOne(targetEntity="Media", cascade={"persist", "remove"})
     * @ORM\JoinColumn(onDelete="SET NULL")
     */
    private $rib;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set sex
     *
     * @param string $sex
     *
     * @return User
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return string
     */
    public function getSex()
    {
        return $this->sex;
    }



    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set tel
     *
     * @param string $tel
     *
     * @return User
     */
    public function setTel($tel)
    {
        $this->tel = $tel;

        return $this;
    }

    /**
     * Get tel
     *
     * @return string
     */
    public function getTel()
    {
        return $this->tel;
    }

    /**
     * Set duree
     *
     * @param integer $duree
     *
     * @return User
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return int
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return User
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }
    /**
     * Set specialite
     *
     * @param \AdminBundle\Entity\Specialite $specialite
     *
     * @return User
     */
    public function setSpecialite(\AdminBundle\Entity\Specialite $specialite = null)
    {
        $this->specialite = $specialite;

        return $this;
    }

    /**
     * Get specialite
     *
     * @return \AdminBundle\Entity\Specialite
     */
    public function getSpecialite()
    {
        return $this->specialite;
    }

    

    /**
     * Set numrib
     *
     * @param integer $numrib
     *
     * @return User
     */
    public function setNumrib($numrib)
    {
        $this->numrib = $numrib;

        return $this;
    }

    /**
     * Get numrib
     *
     * @return integer
     */
    public function getNumrib()
    {
        return $this->numrib;
    }

    /**
     * Set edinar
     *
     * @param integer $edinar
     *
     * @return User
     */
    public function setEdinar($edinar)
    {
        $this->edinar = $edinar;

        return $this;
    }

    /**
     * Get edinar
     *
     * @return integer
     */
    public function getEdinar()
    {
        return $this->edinar;
    }

    /**
     * Set cv
     *
     * @param \AdminBundle\Entity\Media $cv
     *
     * @return User
     */
    public function setCv(\AdminBundle\Entity\Media $cv = null)
    {
        $this->cv = $cv;

        return $this;
    }

    /**
     * Get cv
     *
     * @return \AdminBundle\Entity\Media
     */
    public function getCv()
    {
        return $this->cv;
    }

    /**
     * Set diplome
     *
     * @param \AdminBundle\Entity\Media $diplome
     *
     * @return User
     */
    public function setDiplome(\AdminBundle\Entity\Media $diplome = null)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Get diplome
     *
     * @return \AdminBundle\Entity\Media
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * Set cin
     *
     * @param \AdminBundle\Entity\Media $cin
     *
     * @return User
     */
    public function setCin(\AdminBundle\Entity\Media $cin = null)
    {
        $this->cin = $cin;

        return $this;
    }

    /**
     * Get cin
     *
     * @return \AdminBundle\Entity\Media
     */
    public function getCin()
    {
        return $this->cin;
    }

    /**
     * Set rib
     *
     * @param \AdminBundle\Entity\Media $rib
     *
     * @return User
     */
    public function setRib(\AdminBundle\Entity\Media $rib = null)
    {
        $this->rib = $rib;

        return $this;
    }

    /**
     * Get rib
     *
     * @return \AdminBundle\Entity\Media
     */
    public function getRib()
    {
        return $this->rib;
    }

    /**
     * Set spec
     *
     * @param string $spec
     *
     * @return User
     */
    public function setSpec($spec)
    {
        $this->spec = $spec;

        return $this;
    }

    /**
     * Get spec
     *
     * @return string
     */
    public function getSpec()
    {
        return $this->spec;
    }

    /**
     * Set photo
     *
     * @param \AdminBundle\Entity\Media $photo
     *
     * @return User
     */
    public function setPhoto(\AdminBundle\Entity\Media $photo = null)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return \AdminBundle\Entity\Media
     */
    public function getPhoto()
    {
        return $this->photo;
    }
}
