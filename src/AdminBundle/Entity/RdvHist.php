<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * RdvHist
 *
 * @ORM\Table(name="rdv_hist", indexes={@ORM\Index(name="id_rdv", columns={"id_rdv"}), @ORM\Index(name="user_decroche", columns={"user_decroche"})})
 * @ORM\Entity
 */
class RdvHist
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_start", type="datetime", nullable=true)
     */
    private $dateStart;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_end", type="datetime", nullable=true)
     */
    private $dateEnd;

    /**
     * @var \Rdv
     *
     * @ORM\ManyToOne(targetEntity="Rdv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="id_rdv", referencedColumnName="id")
     * })
     */
    private $idRdv;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_decroche", referencedColumnName="id")
     * })
     */
    private $userDecroche;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set dateStart
     *
     * @param \DateTime $dateStart
     *
     * @return RdvHist
     */
    public function setDateStart($dateStart)
    {
        $this->dateStart = $dateStart;

        return $this;
    }

    /**
     * Get dateStart
     *
     * @return \DateTime
     */
    public function getDateStart()
    {
        return $this->dateStart;
    }

    /**
     * Set dateEnd
     *
     * @param \DateTime $dateEnd
     *
     * @return RdvHist
     */
    public function setDateEnd($dateEnd)
    {
        $this->dateEnd = $dateEnd;

        return $this;
    }

    /**
     * Get dateEnd
     *
     * @return \DateTime
     */
    public function getDateEnd()
    {
        return $this->dateEnd;
    }

    /**
     * Set idRdv
     *
     * @param \AdminBundle\Entity\Rdv $idRdv
     *
     * @return RdvHist
     */
    public function setIdRdv(\AdminBundle\Entity\Rdv $idRdv = null)
    {
        $this->idRdv = $idRdv;

        return $this;
    }

    /**
     * Get idRdv
     *
     * @return \AdminBundle\Entity\Rdv
     */
    public function getIdRdv()
    {
        return $this->idRdv;
    }

    /**
     * Set userDecroche
     *
     * @param \AdminBundle\Entity\User $userDecroche
     *
     * @return RdvHist
     */
    public function setUserDecroche(\AdminBundle\Entity\User $userDecroche = null)
    {
        $this->userDecroche = $userDecroche;

        return $this;
    }

    /**
     * Get userDecroche
     *
     * @return \AdminBundle\Entity\User
     */
    public function getUserDecroche()
    {
        return $this->userDecroche;
    }
}
