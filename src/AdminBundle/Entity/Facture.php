<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Facture
 *
 * @ORM\Table(name="facture", indexes={@ORM\Index(name="user_id", columns={"user_id"}), @ORM\Index(name="rdv_id", columns={"rdv_id"})})
 * @ORM\Entity
 */
class Facture
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float", precision=10, scale=0, nullable=false)
     */
    private $prix;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duree", type="time", nullable=false)
     */
    private $duree;

    /**
     * @var integer
     *
     * @ORM\Column(name="numfact", type="integer", nullable=false)
     */
    private $numfact;

    /**
     * @var \Rdv
     *
     * @ORM\ManyToOne(targetEntity="Rdv")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="rdv_id", referencedColumnName="id")
     * })
     */
    private $rdv;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return Facture
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set duree
     *
     * @param \DateTime $duree
     *
     * @return Facture
     */
    public function setDuree($duree)
    {
        $this->duree = $duree;

        return $this;
    }

    /**
     * Get duree
     *
     * @return \DateTime
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Set numfact
     *
     * @param integer $numfact
     *
     * @return Facture
     */
    public function setNumfact($numfact)
    {
        $this->numfact = $numfact;

        return $this;
    }

    /**
     * Get numfact
     *
     * @return integer
     */
    public function getNumfact()
    {
        return $this->numfact;
    }

    /**
     * Set rdv
     *
     * @param \AdminBundle\Entity\Rdv $rdv
     *
     * @return Facture
     */
    public function setRdv(\AdminBundle\Entity\Rdv $rdv = null)
    {
        $this->rdv = $rdv;

        return $this;
    }

    /**
     * Get rdv
     *
     * @return \AdminBundle\Entity\Rdv
     */
    public function getRdv()
    {
        return $this->rdv;
    }

    /**
     * Set user
     *
     * @param \AdminBundle\Entity\User $user
     *
     * @return Facture
     */
    public function setUser(\AdminBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AdminBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
