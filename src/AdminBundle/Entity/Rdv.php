<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rdv
 *
 * @ORM\Table(name="rdv", indexes={@ORM\Index(name="particulier_id", columns={"particulier_id"}), @ORM\Index(name="seance_id", columns={"seance_id"}), @ORM\Index(name="prof_id", columns={"prof_id"})})
 * @ORM\Entity
 */
class Rdv
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payee", type="boolean", nullable=true)
     */
    private $payee;

    /**
     * @var string
     *
     * @ORM\Column(name="ord", type="text", length=65535, nullable=true)
     */
    private $ord;

    /**
     * @var boolean
     *
     * @ORM\Column(name="part_present", type="boolean", nullable=true)
     */
    private $partPresent;

    /**
     * @var boolean
     *
     * @ORM\Column(name="prof_present", type="boolean", nullable=true)
     */
    private $profPresent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="remain", type="time", nullable=true)
     */
    private $remain;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="particulier_id", referencedColumnName="id")
     * })
     */
    private $particulier;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="prof_id", referencedColumnName="id")
     * })
     */
    private $prof;

    /**
     * @var \Seance
     *
     * @ORM\ManyToOne(targetEntity="Seance")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="seance_id", referencedColumnName="id")
     * })
     */
    private $seance;

    /**
     * @var \Date
     *
     * @ORM\Column(name="date", type="date", nullable=true)
     */
    private $date;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set payee
     *
     * @param boolean $payee
     *
     * @return Rdv
     */
    public function setPayee($payee)
    {
        $this->payee = $payee;

        return $this;
    }

    /**
     * Get payee
     *
     * @return boolean
     */
    public function getPayee()
    {
        return $this->payee;
    }

    /**
     * Set ord
     *
     * @param string $ord
     *
     * @return Rdv
     */
    public function setOrd($ord)
    {
        $this->ord = $ord;

        return $this;
    }

    /**
     * Get ord
     *
     * @return string
     */
    public function getOrd()
    {
        return $this->ord;
    }

    /**
     * Set partPresent
     *
     * @param boolean $partPresent
     *
     * @return Rdv
     */
    public function setPartPresent($partPresent)
    {
        $this->partPresent = $partPresent;

        return $this;
    }

    /**
     * Get partPresent
     *
     * @return boolean
     */
    public function getPartPresent()
    {
        return $this->partPresent;
    }

    /**
     * Set profPresent
     *
     * @param boolean $profPresent
     *
     * @return Rdv
     */
    public function setProfPresent($profPresent)
    {
        $this->profPresent = $profPresent;

        return $this;
    }

    /**
     * Get profPresent
     *
     * @return boolean
     */
    public function getProfPresent()
    {
        return $this->profPresent;
    }

    /**
     * Set remain
     *
     * @param \DateTime $remain
     *
     * @return Rdv
     */
    public function setRemain($remain)
    {
        $this->remain = $remain;

        return $this;
    }

    /**
     * Get remain
     *
     * @return \DateTime
     */
    public function getRemain()
    {
        return $this->remain;
    }

    /**
     * Set particulier
     *
     * @param \AdminBundle\Entity\User $particulier
     *
     * @return Rdv
     */
    public function setParticulier(\AdminBundle\Entity\User $particulier = null)
    {
        $this->particulier = $particulier;

        return $this;
    }

    /**
     * Get particulier
     *
     * @return \AdminBundle\Entity\User
     */
    public function getParticulier()
    {
        return $this->particulier;
    }

    /**
     * Set prof
     *
     * @param \AdminBundle\Entity\User $prof
     *
     * @return Rdv
     */
    public function setProf(\AdminBundle\Entity\User $prof = null)
    {
        $this->prof = $prof;

        return $this;
    }

    /**
     * Get prof
     *
     * @return \AdminBundle\Entity\User
     */
    public function getProf()
    {
        return $this->prof;
    }

    /**
     * Set seance
     *
     * @param \AdminBundle\Entity\Seance $seance
     *
     * @return Rdv
     */
    public function setSeance(\AdminBundle\Entity\Seance $seance = null)
    {
        $this->seance = $seance;

        return $this;
    }

    /**
     * Get seance
     *
     * @return \AdminBundle\Entity\Seance
     */
    public function getSeance()
    {
        return $this->seance;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Rdv
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }
}
