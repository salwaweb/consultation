<?php

namespace AppBundle\Controller;
use AdminBundle\Entity\Seance;
use AdminBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
      /*  if (true == $this->get('security.authorization_checker')->isGranted('ROLE_PRO')) {


            return $this->redirectToRoute('pro_homepage');

        }
      */
        if (true == $this->get('security.authorization_checker')->isGranted('ROLE_USER')){
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            return $this->render('default/index.html.twig',array(
                'user'=>$user->getId()
            ));
        }
        elseif (true == $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN')){
            return $this->redirectToRoute('gestion_homepage');
        }

        // replace this example code with whatever you need
        return $this->render('default/index.html.twig',array(
            'user'=> 0
        ));
    }
    /**
     * @Route("/{rdv_id}/call", name="call-video")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function callAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $rdv = $em->getRepository("AdminBundle:Rdv")->find($request->get("rdv_id"));
        if($rdv == null){
            //handle exeption

        }
        return $this->render('ParticulierBundle:Default:video-call.html.twig',
            array("usertocall" => $rdv->getParticulier()->getId(), "rdv_id" => $request->get("rdv_id")));
    }

    /**
     * @Route("/nos-professionnels", name="nos-professionnels")
     */
    public function listproAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
         $query = $em
            ->createQuery(
                'SELECT u FROM AdminBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_PRO"%');

        $users = $query->getResult();
       // dd($users);
        return $this->render('default/list-pro.html.twig', array(
            'users' => $users,
            'user'=> 0
        ));
    }
    /**
     * @Route("/pro/detail/{id}", name="detail-pro")
     */
    public function detailAction(User $user)
    {


        return $this->render('default/detail.html.twig', array(
            'user' => $user,

        ));
    }
    /**
     * @Route("/pro/planning/{id}", name="detail-planning")
     */
    public function planAction(Request $request,$id)
    {
        $userid = $request->get("id");


        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository("AdminBundle:User")->find( $userid);

        $duree=$user->getDuree();

        // $user = $em->getRepository("AdminBundle:User")->find($userid);
        $seance = $em->getRepository("AdminBundle:Seance")->findBy(array("user" => $user),array("day" => "ASC"));
        //dd($seance);
        $now = new \DateTime();
        $day_of_week = $now->format('N');

        $arr = array();
        $temp = array();
        $schedule = array();
        $i =0;
        $count = COUNT($seance);
        foreach($seance as $key=>$value){
           /* $schedule = array();
            $schedule["id"] = $value->getId();
            $schedule["title"] = "seance";
            $schedule["weekday"] = $value->getDay();
            $schedule["color"] = '#ffffff';
            $schedule["isVisible"] = true;
            $schedule["bgColor"] = '#69BB2D';
            $schedule["dragBgColor"] = '#69BB2D';
            $schedule["borderColor"] = '#69BB2D';
            $schedule["calendarId"] = 'logged-workout';
            $schedule["category"] = 'time';

            if($value->getDay() > $day_of_week){
                $dayToAdd = ABS($value->getDay() - $day_of_week);
                $date = strtotime("+".$dayToAdd." day");
                $xx = date('Y-m-d', $date);
                $schedule["start"] = date('Y-m-d', $date)." ". $value->getTime()->format('H:i:s');
                $schedule["end"] = date('Y-m-d', $date)." ". $value->getEnd()->format('H:i:s');
            }else if($value->getDay() == $day_of_week){

                $now = date("Y-m-d");
                $schedule["start"] = $now." ". $value->getTime()->format('H:i:s');
                $schedule["end"] = $now." ". $value->getEnd()->format('H:i:s');
            }else{

                $dayToAdd = (7 - $day_of_week ) + $value->getDay();

                $date = strtotime("+".$dayToAdd." day");
                $xx = date('Y-m-d', $date);
                $schedule["start"] = date('Y-m-d', $date)." ". $value->getTime()->format('H:i:s');
                $schedule["end"] = date('Y-m-d', $date)." ". $value->getEnd()->format('H:i:s');
            }

            $arr[] = $schedule;*/


            $temp["startTime"] = $value->getTime()->format("H:i");
            $temp["endTime"] = $value->getEnd()->format("H:i");
            $temp["mTime"] = "";
            $temp["text"] = "title";
            $temp["weekday"] = $value->getDay();
            $temp["id"] = $value->getId();
            $temp["type"] = $value->getTimeType();
            $i++;
            if(count($schedule) == 0){
                $schedule[] = $temp;
            }else{
                if($schedule[count($schedule)-1]["weekday"] ==  $value->getDay()){
                    $schedule[] = $temp;
                }else{
                    $arr[$schedule[count($schedule)-1]["weekday"]] = $schedule;
                    $schedule = array();
                    $schedule[] = $temp;
                    if($i == $count){
                        $arr[ $value->getDay()] = $schedule;
                    }
                }
            }

            //print_r(json_encode($arr));
            //echo"\n -------------------------------- \n";


           // $all[] = $row;
        }


        $response = json_encode(array('result' => $arr, "result2" => $duree));
        return new Response($response,200, array(
            'Content-Type' => 'application/json'
        ));


    }

    /**
     * @Route("/pro/rendezvous", name="rendezvous")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function rdvAction(Request $request)
    {
        if (true == $this->get('security.authorization_checker')->isGranted('ROLE_PARTICULIER')) {


            return $this->redirectToRoute('particulier_rdv', array('id' => 25));

        }

    }


    /**
     * @Route("/pro/reservation/{idseance}/{dateseance}", name="rendezvousReservation")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function rdvREdirectAction(Request $request)
    {
        $idseance= $request->get('idseance');
        $dateseance= $request->get('dateseance');

        $em = $this->getDoctrine()->getManager();
        $part = $this->container->get('security.token_storage')->getToken()->getUser();
        $seance = $em->getRepository('AdminBundle:Seance')->findOneBy(array('id'=>$idseance));
        //dd($idseance);
        $message = null;

        return $this->render('ParticulierBundle:Default:rdv.html.twig', array(
            'seance' => $seance,
            'part' => $part,
            'message'=>$message,
            'dateseance'=>$dateseance
        ));
       //return $this->redirectToRoute('particulier_rdv', array('id' => $idseance, "seanceDate" => $dateseance));



    }

    /**
     * @Route("/register/particulier", name="register-particulier")
     */

    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('ParticulierBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setRoles(array('ROLE_PARTICULIER'));
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('homepage');
        }

        return $this->render('ParticulierBundle:Default:register.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }


    /**
     * @Route("/register/professionnel", name="register-professionnel")
     */
    public function newAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AdminBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setRoles(array('ROLE_PRO'));
            $user->setEnabled(false);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('pro-profil', array('id' => $user->getId()));
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }

}
