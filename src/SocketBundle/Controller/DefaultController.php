<?php

namespace SocketBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
		     
        return $this->render('@Socket/Default/index.html.twig');
    }

    public function notifAction()
    {
        $em = $this->getDoctrine()->getEntityManager();
        //find seance ho will start after 30 min;
        //$categories = $em->getRepository('AdminBundle:Seance')->findBy(array());


        return $this->render('@Socket/Default/adminclient.html.twig');
    }
}
