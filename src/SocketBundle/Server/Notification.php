<?php
namespace SocketBundle\Server;

use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use AdminBundle\Entity\Seance;
use AdminBundle\Entity\Rdv;
use AdminBundle\Entity\RdvHist;
use AdminBundle\Entity\User;

class Notification implements MessageComponentInterface
{

    protected $connections = array();

    protected $container;

    public function __construct(ContainerInterface $container)
    {
        if($this->container == null)
			$this->container = $container;
    }

    /**
     * A new websocket connection
     *
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->connections[] = $conn;
		$retour = array("type" => "login");
        $conn->send(json_encode($retour));
        echo "New connection \n";
    }

    /**
     * Handle message sending
     *
     * @param ConnectionInterface $from
     * @param string $msg
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        $messageData = json_decode(trim($msg));
		print_r($messageData);
		echo"------------------------------- \n";
		
		SWITCH ($messageData->type){
			case 'login':
				echo "\n it's login type \n";
				if(isset($messageData->id)){
					//1st app message with connected user
					$token_user = $messageData->id;
					echo "user connected with id ===>  ". $token_user;
					$all_connections = $this->connections;
					foreach($all_connections as $key => $conn){
						if($conn === $from){
							$this->connections[$token_user] = $from;
							$retour = array("type" => "login");
							//$from->send(json_encode($retour));
							//$from->send('..:: Connected as '.$token_user.'  ::..');
							unset($this->connections[$key]);
							break;
						} else {
							//continue;
							//send to other connection that there is a new user has connected
							$retour = array("type" => "new_user", "userId" => $token_user);
							$conn->send(json_encode($retour));
						}

					}
				} else {
					//error
					//
					$from->send(array("type" => "error-login"));
				}
				break;
			
			case 'notification_send':
				echo "\n***********notification_send**********\n";
				foreach($this->connections as $key => $conn){
					echo $key.' \n ';
					echo $messageData->from.' \n ';
					if($key != $messageData->from){
						$conn->send($msg);
					}
				}
				break;
			
			case 'video-offer':
				//prof call particulier
				echo "--------> ".$messageData->type;
				$to = $messageData->to;
				echo $to;
				$this->sendToUser($to, $messageData);
				//add to table historique de l'appel
				$this->ProfCall($messageData->rdv);
				break;
				
			case 'video-answer':
				//particulier raccrocher l'appel
				echo "--------> ".$messageData->type;
				$to = $messageData->to;
				$fromid = $messageData->to;
				echo $fromid;
				echo "\n --------------------- \n";
				echo $to;
				$this->sendToUser($to, $messageData);
				//add to table historique de l'appel
                echo "befor part accept";
                echo $messageData->rdv;
				$this->partAccept($messageData->rdv);

                echo "after part accept";
				break;
				
			case 'new-ice-candidate':
				echo "-*-*-*-*-*-*-*-* > ".$messageData->type;
				$to = $messageData->to;
				$fromid = $messageData->to;
				echo $fromid;
				echo "\n ************************* \n";
				echo $to;
				
				echo "\n ************************* \n";
				$this->sendToUser($to, $messageData);
				
				
				echo "\n ************************* \n";
				//add to table historique de l'appel
				
				break;
				
			case 'hang-up':
			
				$this->sendToUser($messageData->to, $messageData);
                $this->hungUp($messageData->rdv, $messageData->from);
				break;

            case 'check_for_notifcation':

				$this->getUsersSeance();

				break;
			default:
				echo 'default';
		}
			
    }
	
	/**
     * Send To User
     *
     * @param integer $to
     * @param Array $msg
     */
    public function sendToUser($to, $msg)
    {
		
		echo " \n ";
		if (array_key_exists($to, $this->connections)) 
			$this->connections[$to]->send(json_encode($msg));
		
       /* foreach($this->connections as $key => $conn){
			echo $key;
			if($key == $to){
				echo "sendiing";
				$conn->send(json_encode($msg));
			}
		}*/
    }

    /**
     * A connection is closed
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        foreach($this->connections as $key => $conn_element){
            if($conn === $conn_element){
                unset($this->connections[$key]);
                break;
            }
        }
    }

    /**
     * Error handling
     *
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $conn->send("Error : " . $e->getMessage());
        $conn->close();
    }
	
    /**
     * Get user from email credential
     *
     * @param $email
     * @return false|User
     */
    protected function getUserByEmail($email)
    {
        if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
            return false;
        }

        $em = $this->container->get('doctrine')->getManager();
        $repo = $em->getRepository('UserCrmBundle:User');

        $user = $repo->findOneBy(array('email' => $email));

        if($user && $user instanceof User){
            return $user;
        } else {
            return false;
        }

    }
	
	 /**
     * Search for seance who start after 30 min
     *
     */
    protected function getUsersSeance()
    {
		echo "start";
        $em = $this->container->get('doctrine')->getManager();
        //$seances = $em->getRepository('NotificationBundle:Seance');
		
		$from = new \DateTime();
	    $UTC = new \DateTimeZone("UTC");
		$from->setTimezone( $UTC );
        $from->modify('+ 4350 seconds');
		
		$to = new \DateTime();
		$to->setTimezone( $UTC );
        $to->modify('+ 4650 seconds');
		
		
		$day = $from->format("w");
		
		 			
			  $query3 = $em->createQuery("SELECT a FROM AdminBundle:Rdv a LEFT JOIN AdminBundle:Seance p WITH p.id = a.seance  

				WHERE  ((p.time > :from AND p.time < :to) ) AND p.day = $day")

		   ->setParameter("from", $from->format("H:i"))

		   ->setParameter("to", $to->format("H:i"));



            $rdvs = $query3->getResult();
			
			//$rdvs = $em->getRepository('AdminBundle:Rdv')->findall();
			
			echo "22222222222222";
			
			echo COUNT($rdvs);
			
			foreach($rdvs as $key => $value){
				$part_id = $value->getParticulier()->getId();
				echo $part_id;
				$this->sendToUser($part_id , array("type" => "check_for_notifcation" , "rdvid" => $value->getId()));
				
				$prof_id = $value->getProf()->getId();
				echo " \n ";
				echo "\n -*-*-*-*-*-*-*-*-*-* \n ";
				echo $prof_id;
				$this->sendToUser($prof_id , array("type" => "check_for_notifcation" , "rdvid" => $value->getId()));
				
			}
			
			$this->sendToUser(14 , array("type" => "check_for_notifcation" , "rdvid" => 10));
			$this->sendToUser(8 , array("type" => "check_for_notifcation" , "rdvid" => 10));
				
		
    }

    public function profCall($rdv){

        $em = $this->container->get('doctrine')->getManager();
        $rdvid =  $em->getRepository('AdminBundle:Rdv')->find($rdv);
        $historique = new RdvHist();
        $historique->setIdRdv($rdvid);
        $em->persist($historique);
        $em->flush();
    }

    public function partAccept($rdv){
        echo "\n ----------------------- \n ";
        echo 'particulier accept';
        echo $rdv;
        $em = $this->container->get('doctrine')->getManager();

        echo "111111";
        $idrdv = $em->getRepository('AdminBundle:Rdv')->find($rdv);

        echo "2222222";
        $hist=  $em->getRepository('AdminBundle:RdvHist')->findOneBy(array('idRdv'=>$idrdv),array('id'=>'DESC'));

        echo "33333333";
        $hist->setDateStart(new \DateTime());
        $em->persist($hist);

        echo "444444";
        $em->flush();

        echo "end particulier accept";
    }
    public function hungUp($rdv, $user){

        $em = $this->container->get('doctrine')->getManager();
        $idrdv = $em->getRepository('AdminBundle:Rdv')->find($rdv);
        $userr = $em->getRepository('AdminBundle:User')->find($user);
        $hist=  $em->getRepository('AdminBundle:RdvHist')->findOneBy(array('idRdv'=>$idrdv)
        ,array('id'=>'DESC'));
        $hist->setUserDecroche($userr);
        $hist->setDateEnd(new \DateTime());
        $em->persist($hist);
        $em->flush();
    }
}