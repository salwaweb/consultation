<?php

namespace GestionBundle\Controller;
use AdminBundle\Entity\User;
use AdminBundle\Entity\Specialite;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{


    public function planningAction(User $user)
    {
        // replace this example code with whatever you need

        return $this->render('GestionBundle:Default:planning.html.twig',array(
            'user'=>$user
        ));
    }
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
         $query = $em
            ->createQuery(
                'SELECT u FROM AdminBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_PRO"%');

        $profs = $query->getResult();
        $queryp = $em
            ->createQuery(
                'SELECT u FROM AdminBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_PARTICULIER"%');
        // dd($users);
        $parts = $queryp->getResult();
        $specialites = $em->getRepository('AdminBundle:Specialite')->findAll();
        $rdvs = $em->getRepository('AdminBundle:Rdv')->findAll();
        return $this->render('GestionBundle:Default:index.html.twig', array(
            'profs' => $profs,
            'parts' => $parts,
            'rdvs' => $rdvs,
            'specialites' => $specialites
        ));

    }


    public function listspecAction()
    {
        $em = $this->getDoctrine()->getManager();

        $specialites = $em->getRepository('AdminBundle:Specialite')->findAll();

        return $this->render('GestionBundle:Default:liste-specialites.html.twig', array(

            'specialites' => $specialites
        ));

    }

    /**
     * Creates a new user entity.
     *
     */
    public function addspecAction(Request $request)
    {
        $specialite = new Specialite();
        $form = $this->createForm('AdminBundle\Form\SpecialiteType', $specialite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $specialite->setStatus(true);
            $em->persist($specialite);
            $em->flush();
            return $this->redirectToRoute('gestion_specilalite');
        }

        return $this->render('GestionBundle:Default:addspecialite.html.twig', array(
            'specialite' => $specialite,
            'form' => $form->createView(),
        ));
    }
    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function actspecAction(Request $request, Specialite $id)
    {
        $em= $this->getDoctrine()->getManager();

        $act = $em->getRepository('AdminBundle:Specialite')->findOneBy(array('id' => $id));

        $act->setStatus(true);

        $em->persist( $act); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('gestion_specilalite');

    }
    public function bloqspecAction(Request $request, Specialite $id)
    {
        $em= $this->getDoctrine()->getManager();

        $act = $em->getRepository('AdminBundle:Specialite')->findOneBy(array('id' => $id));

        $act->setStatus(false);

        $em->persist( $act); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('gestion_specilalite');

    }

    /**
     * Creates a new user entity.
     *
     */
    public function addproAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('AdminBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setRoles(array('ROLE_PRO'));
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('gestion_professionel');
        }

        return $this->render('GestionBundle:Default:add-pro.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }


    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function bloqAction(Request $request, User $id)
    {
        $em= $this->getDoctrine()->getManager();

        $act = $em->getRepository('AdminBundle:User')->findOneBy(array('id' => $id));

        $act->setEnabled(false);

        $em->persist( $act); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('gestion_professionel');

    }
    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function editpwdAction(Request $request, User $user)
    {

        $editForm = $this->createForm('GestionBundle\Form\UserPassType', $user);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('gestion_homepage');
        }

        return $this->render('GestionBundle:Default:modif-pwd.html.twig', array(
            'user' => $user,
            'edit_form' => $editForm->createView(),

        ));

    }
    public function listplanAction()
    {
        $em = $this->getDoctrine()->getManager();



        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AdminBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_PRO"%');

        $users = $query->getResult();
        // dd($users);
        return $this->render('GestionBundle:Default:liste-planning.html.twig', array(
            'users' => $users,
        ));


    }

    public function listproAction()
    {
        $em = $this->getDoctrine()->getManager();



        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AdminBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_PRO"%');

        $users = $query->getResult();
       // dd($users);
        return $this->render('GestionBundle:Default:liste-pro.html.twig', array(
            'users' => $users,
        ));


    }
    public function listpartAction()
    {
        $em = $this->getDoctrine()->getManager();



        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery(
                'SELECT u FROM AdminBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_PARTICULIER"%');

        $users = $query->getResult();
        // dd($users);
        return $this->render('GestionBundle:Default:liste-part.html.twig', array(
            'users' => $users,
        ));


    }


}
