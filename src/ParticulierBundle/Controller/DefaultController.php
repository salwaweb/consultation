<?php

namespace ParticulierBundle\Controller;

use AdminBundle\Entity\User;
use AdminBundle\Entity\Seance;
use AdminBundle\Entity\Rdv;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;


class DefaultController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        $rdvs = $em->getRepository('AdminBundle:Rdv')->findBy(array('particulier'=>$user));
        return $this->render('ParticulierBundle:Default:index.html.twig',array(
            'rdvs'=>$rdvs,
            'user'=>$user->getId()
        ));
    }

    public function rdvAction( Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $part = $this->container->get('security.token_storage')->getToken()->getUser();
        $seance = $em->getRepository('AdminBundle:Seance')->find(7);
        $message = null;

        return $this->render('ParticulierBundle:Default:rdv.html.twig', array(
            'seance' => $seance,
            'part' => $part,
            'message'=>$message
        ));
    }

    public function addrendevAction( Request $request)
    {
        $idseance= $request->get('idseance');
        $dateseance= $request->get('dateseance');
       // dd($dateseance);
        $em = $this->getDoctrine()->getManager();
        $part = $this->container->get('security.token_storage')->getToken()->getUser();
        $seance = $em->getRepository('AdminBundle:Seance')->find($idseance);

        $prof= $seance->getUser();

        $rdv = new Rdv();
        $rdv->setParticulier($part);
        $rdv->setProf($prof);
        $rdv->setSeance($seance);
        $rdv->setDate(new \DateTime($dateseance));
        $rdv->setPayee(true);
        //$rdv->setOrd($request->get('ordonnance'));
        $rdv->setPartPresent(false);
        $rdv->setProfPresent(false);
        $rdv->setRemain(new \DateTime());
        $em->persist($rdv);
        $em->flush();
        $message= (" Votre réservation est confirmée.!");
        return $this->render('ParticulierBundle:Default:rdv.html.twig', array(
            'seance' => $seance,
            'part' => $part,
            'message'=>$message,
            'dateseance'=>$dateseance
        ));
    }



    /**
     * Creates a new user entity.
     *
     */
    public function registerAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm('ParticulierBundle\Form\UserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user->setRoles(array('ROLE_PARTICULIER'));
            $user->setEnabled(false);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('user_show', array('id' => $user->getId()));
        }

        return $this->render('user/new.html.twig', array(
            'user' => $user,
            'form' => $form->createView(),
        ));
    }
    /**
     * Displays a form to edit an existing user entity.
     *
     */
    public function pwdAction(Request $request, User $id)
    {
        $pwd= $request->get('pwd');
        $cpwd= $request->get('cpwd');
        $em= $this->getDoctrine()->getManager();

        $act = $em->getRepository('AdminBundle:User')->findOneBy(array('id' => $id));

        $act->setPassword($pwd);

        $act->setPlainPassword($cpwd);

        $em->persist( $act); // On persist (création ou modification)

        $em->flush();

        return $this->redirectToRoute('particulier_homepage');

    }



    public function listAction()
    {
        $em = $this->getDoctrine()->getManager();
        $query = $em
            ->createQuery(
                'SELECT u FROM AdminBundle:User u WHERE u.roles LIKE :role'
            )->setParameter('role', '%"ROLE_PRO"%');

        $profs = $query->getResult();
        return $this->render('ParticulierBundle:Default:list-pro.html.twig');
    }

}
